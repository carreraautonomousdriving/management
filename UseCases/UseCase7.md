# 7. Name Track
## 7.1 Brief description

Give the built track a name.

## 7.2 Mock up screenshots

![screenshot-track-design](grid_design.png "Track Design")

## 7.3 Flow of events

- Click in the text field of the 'Controller' box
- Type in the name you want to give the track

## 7.4 Activity diagram

![screenshot-name-track](name_track.png "Name Track")

## 7.5 The .feature file

There is no feature file to this use case. See the descriptions of the other use cases for feature files.

## 7.6 Related use cases

- Track Design
- Save Track
- Validate Track

## 7.7 Preconditions

User interface shows 'Track Design' and a track was built.

## 7.8 Postconditions
A name is typed in the text field.

## 7.9 Function points
![functionpoint-nametrack](Function%20Points/FP_-_Name_Track.png "Function Points Name Track")