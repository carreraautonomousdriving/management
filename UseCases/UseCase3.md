# 3 Race configuration
## 3.1 Brief description
The user can configure the controls for the race, like lap counter or difficulty level.
## 3.2 Mock up screenshots


![screenshot-racesettings](race_settings.png "Race Settings")

## 3.3 Flow of events

- Click on 'Race Settings'
- Choose difficulty settings
- Insert a number in the lap counter (0 equals infinity)
 
## 3.4 Activity diagram

![screenshot-race-configuration](race_configuration.png "Race Configuration")

## 3.5 The .feature file
The unchanged .feature file from the first semester shows an easily readable code for this usecase.

![screenshot-RCfeaturefile](Feature-Race_Configuration.png "Race Configuration als .feature")

## 3.6 Related use cases

- Start race
- Load saved tracks

## 3.7 Preconditions

User interface shows 'Overview' or 'Track Design'.

## 3.8 Postconditions

After everything is set, the user can go to 'Start race'.

## 3.9 Function points

![functionpoint-raceconfiguration](Function%20Points/FP_-_race_configuration.png "Function Race Configuration")
