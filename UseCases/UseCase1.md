# 1. Track Design
## 1.1 Brief description
The goal for the user is to virtually recreate the track he previously built in real life.  On a userinterface, he can click the track parts one after the other to recreate his desired track.
## 1.2    Mock up screenshots

![screenshot-overview](overview.png "Overview")

![screenshot-griddesign](grid_design.png "Grid Design")

![screenshot-race_settings](race_settings.png "Race Settings")


## 1.3. Flow of events

- Click on "Grid Design"
- First click on the start panel from the track toolbar to start building a track.
- Click on the track parts one after the other until the track is complete.
- If you want to delete the last added track part, you can click on the button 'Remove'.
- If your want to delete every added track part, you can click on the button 'Clear Track'.

## 1.4 Activity diagram

![screenshot-trackdesign](track_design.png "Track Design")

## 1.5 The .feature file
The unchanged .feature file from the first semester shows an easily readable code for this usecase.

![screenshot-TDfeaturefile](Feature-Track_Design.png "Track Design als .feature")

## 1.6 Related use cases

 - Load saved tracks
 - Save tracks
 - Validate track

## 1.7 Preconditions

User interface shows 'Overview' or 'Race settings'.

## 1.8 Postconditions

After successfully creating the track, the red help line should not be interrupted

## 1.9 Function points

![functionpoint-trackdesign](Function%20Points/FP_-_track_design.png "Function Track Design")
