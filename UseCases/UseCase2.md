# 2. Start race
## 2.1    Brief description
The user can start a race on his chosen track against the computer. 
## 2.2    Mock up screenshots

![screenshot-overview](race_settings.png "Race Settings")


## 2.3 Flow of events

- Choose a track.
- Click on the 'Load Configuration' button to load the chosen configuration on the Raspberry Pi.
- Click on the start button on the Rasperry Pi to start the race.

## 2.4 Activity diagram

![screenshot-start-race](start_race.png "Start Race")

## 2.5 The .feature file
The unchanged .feature file from the first semester shows an easily readable code for this usecase.

![screenshot-SRfeaturefile](Feature-Start_Race.png "Start Race als .feature")

## 2.6 Related use cases

 - Race configuration
 - Emergency state

## 2.7 Preconditions

 User interface shows 'Race Settings' with the selected configuration. 
 
## 2.8 Postconditions

A countdown with red LEDs is shown. When they turn green the race will start. As long as the race is going, the LEDs will shine.

## 2.9 Function points

![functionpoint-startrace](Function%20Points/FP_-_start_race.png "Function Start Race")
