# 4 Save track
## 4.1 Brief description
The user can save the created track for later use.
## 4.2 Mock up screenshots

![screenshot-grid-design](grid_design.png "Track Design")

## 4.3 Flow of events

 - If you are satisfied with your valid track click on button 'Save'.
 

## 4.4 Activity diagram

![screenshot-save-track](save_track.png "Save Track")

## 4.5 The .feature file
The feature file to this use case is not up to date due to changes on this use case during development.

![screenshot-LSTfeaturefile](Feature-Manage_Saved_Track.png "Manage saved track als .feature")

## 4.6 Related use cases

 - Track Design
 - Start race
 - Validate Track

## 4.7 Preconditions

User interface shows 'Track Design' and a track is built.

## 4.8 Postconditions

The Track is saved to the Raspberry Pi and the UI shows 'Race Configuration'.

## 4.9 Function points

![functionpoint-savetracks](Function%20Points/FP_-_Save_Track.png "Function Point Save Track")
