# 5. Emergency state
## 5.1 Brief description
Stop the race after a crash to not risk any further crashes and to give the crashee a chance to re-enter.

## 5.2 Mock up screenshots

![screenshot-race_settings](race_settings.png "Race Settings")

## 5.3 Flow of events

- Crash your car.
- Press the stop button on the Raspberry Pi.
- Get your car back on the finish line.
- Start driving again.

## 5.4 Activity diagram

![screenshot-emergeny-state](stop_race.png "Stop Race")

## 5.5 The .feature file
The unchanged .feature file from the first semester shows an easily readable code for this usecase.

![screenshot-CPfeaturefile](Feature-Stop_Race.png "Stop Race als .feature")

## 5.6 Related use cases

Start race

## 5.7 Preconditions

A race is already running.

## 5.8 Postconditions

Start a new race.

## 5.9 Function points

![functionpoint-overview](Function%20Points/cautionPeriodFP.PNG "Function Overview")
