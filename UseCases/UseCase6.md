# 6. Validate Track
## 6.1 Brief description

Let the System validate your created track.

## 6.2 Mock up screenshots

![screenshot-trackdesign](grid_design.png "Track Design")

## 6.3 Flow of events

- Be satisfied with the track you created.
- Be unsure if your track is executable.
- Click on the  button 'Validate'.

## 6.4 Activity diagram

![screenshot-validate-track](validate_track.png "Validate Track")

## 6.5 The .feature file
There is no feature file to this use case. See the descriptions of the other use cases for feature files.

## 6.6 Related use cases

Track design

## 6.7 Preconditions

User interface shows 'Track Design' and a track was built.

## 6.8 Postconditions
Below the button 'Validate' a message is shown to inform the user.

## 6.9 Function points

![functionpoint-validatetrack](Function%20Points/FP_-_Validate_Track.png "Function Points Validate Track")