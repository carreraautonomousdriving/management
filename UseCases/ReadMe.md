# Use Cases
## [1. Design Track](https://gitlab.com/carreraautonomousdriving/management/-/blob/master/UseCases/UseCase1.md)
## [2. Start race](https://gitlab.com/carreraautonomousdriving/management/-/blob/master/UseCases/UseCase2.md)
## [3. Race configuration](https://gitlab.com/carreraautonomousdriving/management/-/blob/master/UseCases/UseCase3.md)
## [4. Manage saved tracks](https://gitlab.com/carreraautonomousdriving/management/-/blob/master/UseCases/UseCase4.md)
## [5. Caution period](https://gitlab.com/carreraautonomousdriving/management/-/blob/master/UseCases/UseCase5.md)
## [6. Validate Track](https://gitlab.com/carreraautonomousdriving/management/-/blob/master/UseCases/UseCase6.md)
## [7. Name Track](https://gitlab.com/carreraautonomousdriving/management/-/blob/master/UseCases/UseCase7.md)
