<object data="https://gitlab.com/carreraautonomousdriving/management/Test/Testplan.pdf" type="application/pdf" width="700px" height="700px">
    <embed src="https://gitlab.com/carreraautonomousdriving/management/Test/Testplan.pdf">
        <p>This browser does not support PDFs. Please download the PDF to view it: <a href="https://gitlab.com/carreraautonomousdriving/management/-/blob/master/Test/Testplan.pdf">Download PDF</a>.</p>
    </embed>
</object>