# Software Architecture Document


## 1. Introduction 
### 1.1 Purpose
This document provides a comprehensive architectural overview of the system, 
using a number of different architectural views to display different aspects of the system. 
It is intended to capture and convey the significant architectural decisions which have been made on the system.

### 1.2 Scope
The scope of this SAD is to display the architectue of our Arerrac project. Affected is MVC of the TrackDesigner.

### 1.3 References
- [GitLab](https://gitlab.com/carreraautonomousdriving)
- [Blog](https://arerrac.wordpress.com/)
- [Software Requirements Specification](https://gitlab.com/carreraautonomousdriving/management/blob/master/README.md)

## 2. Architectural Representation
Arerrac will use the MVC-Pattern for the TrackDesigner App, even though it is called MVT in Django, the system is mostly the same.
We are only able to implement it in this subproject.
You can see the whole application design below:



## 3. Architectural Goals and Constraints 
We decided to use Java-Script for the track-desginer app, as it should be possible to implement its usecase with low afford. 
We have considered the drawback of requiring a windows device to create the tracks once, but you are only required to use it in the beginning.
To control the race settings we decided to create a webpage, which is useable by mostly every device and used more frequent than the windows app, because you dont recreate your track often.
By this, the user will have a only one platform to worry about.

## 4. Use-Case View 
[See in the Use-Case Readme]()

## 5. Logical View
### 5.1 Overview

![APP-Architecture](./Architecture.png)

### 5.2 Architecturally Significant Design Packages
The following Code features the Command Pattern:

![APP-Pattern](./Patterns_Prev.png)

## 6. Process View

## 7. Deployment View
The Deployment View can be seen in 5.1.
The Deployment on the Raspberry Pi is fully automated, you just need to download a git repository and execute the install script:
![Deployment](./Automation.png)

## 8. Implementation View

## 9. Data View
Our Data will be saved in a SQLite Database on the raspberry pi within its corresponding part.
The Race controller will have access to it.

Data on the Frontend:

![APP-Architecture](./Patterns_Prev.png)

![Database](./dataview.png)

Data between Frontend and Backend:

![APP-Architecture](./ClassDiagramm.png)




## 10. Size and Performance

## 11. Quality
We will insure our webpage will work in any browser, by checking every release in the major browser (Chrome, Firefox, Opera & Edge). 
We will drop support for Internet Explorer, because Microsoft is dropping it soon too and is only being used by big companies, which we dont target.
