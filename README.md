# Arerrac Management

##### In this project we will describe any management relevant information.

## Software Requirements Specification 
### 1.	Introduction
##### The project is split in four parts:
##### - [WebApp](https://gitlab.com/carreraautonomousdriving/WebApp)
##### - [Track Designer](https://gitlab.com/carreraautonomousdriving/track-designer)
##### - [Appliance Install](https://gitlab.com/carreraautonomousdriving/appliance-install)
##### - [Track Controller](https://gitlab.com/carreraautonomousdriving/track-controller)

You can find the issue list and time tracking on our [YouTrack](https://dhbw-karlsruhe.myjetbrains.com/youtrack/issues/ARERRAC)

#### 1.1 Purpose
Our SRS describes the external behavior of the application. 
In here is specified how the application will interact with the user. 
You can find defined responsetimes and a user manual.
<!--
[Specify the purpose of this SRS. The SRS fully describes the external behavior of the application or subsystem identified. It also describes nonfunctional requirements, design constraints, and other factors necessary to provide a complete and comprehensive description of the requirements for the software.]
-->

#### 1.2 Scope
Our webbased frontend will have a track designer, which gives the possibility to virtually create your track. It also features a race configurator. In it you can configure how many rounds you want to drive on a given track.
You will be able to manage all of your tracks and load them to the autonomous driving controller. With the Hardware Control Buttons the user can start the preconfigured race, or start the caution period.

Use-Case Model:
![Use-Case Model Arerrac](UseCaseDiagram.png)
<!--
[A brief description of the software application that the SRS applies to, the feature or other subsystem grouping, what Use-Case model(s) it is associated with, and anything else that is affected or influenced by this document.]
 -->

#### 1.3 Definitions, Acronyms, and Abbreviations
| Abbreviation | Definition                        |
|--------------|-----------------------------------|
| CAD          | Carrera Autonomous Driving        |
| AD           | Autonomous Driving                |
| HTML         | Hyper Text Markup Language        |
| JS           | Java-Script                       |
| CSS          | Cascadian Style Sheet             |
| AJAX         | Asynchronous Javascript and XML   |
|              |                                   |

<!-- 
[This subsection provides the definitions of all terms, acronyms, and abbreviations required to properly interpret the SRS.  This information may be provided by reference to the project’s Glossary.]
-->

#### 1.4 References

 - [Use Case Diagramm](https://gitlab.com/carreraautonomousdriving/management/blob/master/UseCases/ReadMe.md)
 - [Software Architecture Diagramm](https://gitlab.com/carreraautonomousdriving/management/blob/master/SAD/README.md)

<!-- 
[This subsection provides a complete list of all documents referenced elsewhere in the SRS.  Identify each document by title, report number if applicable, date, and publishing organization.  Specify the sources from which the references can be obtained. This information may be provided by reference to an appendix or to another document.]
-->

#### 1.5 Overview
tbd
<!--
[This subsection describes what the rest of the SRS contains and explains how the document is organized.]
-->

### 2. Overall Description
tbd
<!-- 
[This section of the SRS describes the general factors that affect the product and its requirements.  This section does not state specific requirements.  Instead, it provides a background for those requirements, which are defined in detail in Section 3, and makes them easier to understand. Include such items as: 
-	product perspective
-	product functions
-	 user characteristics
-	constraints
-	assumptions and dependencies
-	requirements subsets] 
-->

### 3. Specific Requirements 

An Overall Overview can be seen [here](https://gitlab.com/carreraautonomousdriving/management/tree/master/UseCases/ "Link to Overall UseCases") and following each element with a brief description:
 - [Usecase 1](https://gitlab.com/carreraautonomousdriving/management/tree/master/UseCases/UseCase1.md): The user can select one of his formerly saved tracks to play with, as well as deleting it.
 - [Usecase 2](https://gitlab.com/carreraautonomousdriving/management/tree/master/UseCases/UseCase2.md): The user can start a race on his chosen track against the computer.
 - [Usecase 3](https://gitlab.com/carreraautonomousdriving/management/tree/master/UseCases/UseCase3.md): The user can configure the controls for the race, like e.g.  a lap counter or the difficulty level. A picture and other information of his track are shown.
 - [Usecase 4](https://gitlab.com/carreraautonomousdriving/management/tree/master/UseCases/UseCase4.md): The user can select one of his formerly saved tracks to play with, as well as deleting it.
 - [Usecase 5](https://gitlab.com/carreraautonomousdriving/management/tree/master/UseCases/UseCase5.md): After  a  crash,  the  autonomous  car  will  drive  slowly  to  the  finish  line  to  not  risk  any  further crashes and to give the crashee a chance to re-enter the match.
<!--
[This section of the SRS contains all software requirements to a level of detail sufficient to enable designers to design a system to satisfy those requirements, and testers to test that the system satisfies those requirements. When using use-case modeling, these requirements are captured in the Use Cases and the applicable supplementary specifications.  If use-case modeling is not used, the outline for supplementary specifications may be inserted directly into this section, as shown below.]
-->

#### 3.1 Functionality
tbd
<!--
[This section describes the functional requirements of the system for those requirements that are expressed in the natural language style. For many applications, this may constitute the bulk of the SRS package and thought should be given to the organization of this section. This section is typically organized by feature, but alternative organization methods may also be appropriate; for example, organization by user or organization by subsystem. Functional requirements may include feature sets, capabilities, and security.
Where application development tools, such as requirements tools, modeling tools, and the like, are employed to capture the functionality, this section of the document would refer to the availability of that data, indicating the location and name of the tool used to capture the data.]
-->

##### 3.1.1 Functional Requirement One
tbd
<!--
[The requirement description.]
-->

#### 3.2 Usability 
tbd
<!--
[This section includes all those requirements that affect usability. For example,
•	specify the required training time for a normal users and a power user to become productive at particular operations
•	specify measurable task times for typical tasks or base the new system’s usability requirements on other systems that the users know and like
•	specify requirement to conform to common usability standards, such as IBM’s CUA standards Microsoft’s GUI standards]
-->

##### 3.2.1 Usability Requirement One
tbd
<!--
[The requirement description goes here.]
-->

#### 3.3 Reliability 

Availability:                       100% - no maintenance required, because there is no central server, only on hardware failure.
Mean Time Between Failures:         10 years
Mean time to repair:                0 sec, becaue failure only on unfixable hardware error
Accuracy-specifies precision:       tbd (typically 10ms)
Defect Rate:                        2 bugs/KLOC

| Bugtype                                  | Explanation                                             | Categorize  |
|------------------------------------------|---------------------------------------------------------|-------------|
| Racetrack incomplete / incorrect         | pysical track parts not connected correctly             | critical    |
| Virtual racetrack incomplete / incorrect | virtual track parts not connected correctly             | critical    |
| Webserver not responding                 | browser time out                                        | minor       |
| TCP Server not responding                | track designer can't upload to the race controller      | minor       |
| Power outage                             | power failure                                           | significant |
| Racecar variations                       | different driving behavior, lead to wrong calculations  | significant |

<!--
[Requirements for reliability of the system should be specified here. Some suggestions follow:
•	Availability—specify the percentage of time available ( xx.xx%), hours of use, maintenance access, degraded mode operations, and so on.
•	Mean Time Between Failures (MTBF) — this is usually specified in hours, but it could also be specified in terms of days, months or years.
•	Mean Time To Repair (MTTR)—how long is the system allowed to be out of operation after it has failed?
•	Accuracy—specifies precision (resolution) and accuracy (by some known standard) that is required in the system’s output.
•	Maximum Bugs or Defect Rate—usually expressed in terms of bugs per thousand lines of code (bugs/KLOC) or bugs per function-point( bugs/function-point).
•	Bugs or Defect Rate—categorized in terms of minor, significant, and critical bugs: the requirement(s) must define what is meant by a “critical” bug; for example, complete loss of data or a complete inability to use certain parts of the system’s functionality.]
-->

#### 3.4 Performance

The webapplication should responed in a fair amount of time. 
The driving controller should be operable as fast as possible.
<!--
[The system’s performance characteristics are outlined in this section. Include specific response times. Where applicable, reference related Use Cases by name.
•	Response time for a transaction (average, maximum)
•	Throughput, for example, transactions per second
•	Capacity, for example, the number of customers or transactions the system can accommodate
•	Degradation modes (what is the acceptable mode of operation when the system has been degraded in some manner)
•	Resource utilization, such as memory, disk, communications, and so forth.
-->

#### 3.5 Supportability

The webapplication should be useable with typical webbrowser devices like notebooks, tablets and even smartphones.
The track designer should be useable with supported windows clients with .NET Framework 4.5
<!--
[This section indicates any requirements that will enhance the supportability or maintainability of the system being built, including coding standards, naming conventions, class libraries, maintenance access, and maintenance utilities.]
-->

##### 3.6 Design Constraints

At first, we divided the main tasks of the project. Although this is a group project in which everyone is involved in every topic, now every task is managed and organized by one team member. As an integrator, Robin Meckler will be responsible for the web frontend, Pascal Dziersk will manage the project requirements and implement the backend, Vincent Schreck is responsible for test management and test analysis and Leon Gieringer as implementer and environment manager. In the next step every branch needs to get started creating fundamental design patterns. We will use GitLab as our project management tool, as it also features our revision control system.

To manage the Carrera circuit we need an engine control extension board for the Raspberry Pi.
The board features connectivity for two motors, where we will only use one in the current concept. The speed of the engine will be adjusted with pulse width modulation.


![Raspberry Pi with Pololu dual MAX14870 motor driver extension](https://asset.conrad.com/media10/is/160267/19d4690e133380f076daee5ebc9306f73/c3/-/abbce7852b1d442c6880214a8ba3e244c/pololu-dual-max14870-motor-driver-for-raspberry-pi-assembled-802237918.jpg?x=480&y=480&format=jpg&ex=480&ey=480&align=center "Raspberry Pi with Pololu dual MAX14870 motor driver extension")

For the frontend, the user can create his Carrera track on his devices web browser . Via drag and drop he can successively move the virtual parts until his track is fully implemented in the system. For the drag and drop we will use Javascripts build in drag and drop feature.

![first frontend mockup](https://arerrac.files.wordpress.com/2019/10/brave_fmlilnwbd2.png?w=1024 "first frontend mockup")

Our first webinterface to virtually create the track

<!--
[This section indicates any design constraints on the system being built. Design constraints represent design decisions that have been mandated and must be adhered to.  Examples include software languages, software process requirements, prescribed use of developmental tools, architectural and design constraints, purchased components, class libraries, and so on.]
-->

#### 3.7 On-line User Documentation and Help System Requirements
The web application will guide the user through its configurtaion with helpful hints.

#### 3.8 Purchased Components
| Produkt                                | Anzahl | Preis    | Angebot      |
|----------------------------------------|--------|----------|------------|
| Raspberry Pi 4                         | 1      | 62,49 €  | [Link](https://www.conrad.de/de/p/raspberry-pi-4-b-4-gb-4-x-1-5-ghz-raspberry-pi-2138865.html) |
| Raspberry Pi MicroSD                   | 1      | 8,99 €   | [Link](https://www.conrad.de/de/p/samsung-evo-plus-microsdhc-karte-32-gb-class-10-uhs-i-inkl-sd-adapter-1547259.html) |
| Raspberry Pi Netzteil + Kabel          | 1      | 10,99 €  | [Link](https://www.conrad.de/de/p/raspberry-pi-steckernetzteil-festspannung-passend-fuer-raspberry-pi-ausgangsstrom-max-3000-ma-1-x-usb-c-stecker-2140238.html) |
| Spannungsmessgerät                     | 1      | 23,99 €  | [Link](https://www.conrad.de/de/p/voltcraft-vc130-1-hand-multimeter-digital-cat-iii-250-v-anzeige-counts-2000-1090519.html) |
|                                        |        |          |            |
| Carrerbahn                             | 1      | 104,99 € | [Link](https://www.conrad.de/de/p/carrera-20025234-evolution-dtm-speed-duel-start-set-1707388.html) |
| Carerra Erweiterung                    | 1      | 28,99 €  | [Link](https://www.conrad.de/de/p/carrera-20026955-evolution-digital-132-digital-124-digital-ausbauset-1-set-518166.html) |
| Carerra Geraden                        | 1      | 36,99 €  | [Link](https://www.conrad.de/de/p/carrera-20020509-evolution-digital-132-digital-124-standardgeraden-4-st-518132.html) |
|                                        |        |          |            |
| RPI Motor Driver Board PoluluMAX 14780 | 1      | 18,65 €  | [Link](https://www.conrad.de/de/p/pololu-dual-max14870-motor-driver-for-raspberry-pi-assembled-802237918.html) |
| RPI GPIO Port Extension                | 1      | 9,09€    | [Link](https://www.conrad.de/de/p/raspberry-pi-erweiterungs-platine-rb-port-doubler-raspberry-pi-2-b-raspberry-pi-3-b-raspberry-pi-3-b-raspberry-1720611.html) |
| Beschleunigungssensor                  | 1      | 3,99 €   | [Link](https://www.conrad.de/de/p/joy-it-mpu6050-beschleunigungs-sensor-1-st-passend-fuer-micro-bit-arduino-raspberry-pi-rock-pi-banana-pi-c-control-2136256.html) |
| Raspberry Pi Button Modul              | 3      | 2,79 €   | [Link](https://www.conrad.de/de/p/druckschalter-se043-iduino-se043-1616261.html) | 
| Raspberry Pi LED Modul                 | 5      | 2,78 €   | [Link](https://www.conrad.de/de/p/rgb-led-modul-se010-iduino-se010-1616253.html) | 
| Lichtschranke (Empfänger)              | 3      | 4,95 €   | [Link](https://www.conrad.de/de/p/makerfactory-ir-empfaenger-vma317-passend-fuer-arduino-boards-arduino-arduino-uno-fayaduino-freeduino-seeeduino-1612773.html) |
| Lichtschranke (Sender)                 | 3      | 4,95 €   | [Link](https://www.conrad.de/de/p/makerfactory-ir-sender-vma316-passend-fuer-arduino-boards-arduino-arduino-uno-fayaduino-freeduino-seeeduino-see-1612772.html) |                             |                                        |        |          |            |
| Kabel (GPIO)                           | 2      | 6,99 €   | [Link](https://www.conrad.de/de/p/joy-it-rb-cb2-30-jumper-kabel-raspberry-pi-40x-drahtbruecken-buchse-40x-drahtbruecken-buchse-0-30-m-bunt-inkl-pins-1182192.html) | 
| Kabel (Netzteil)                       | 6      | 0,62 €   | [Link](https://www.conrad.de/de/p/faber-kabel-031049-schlauchleitung-h03vvh2-f-2-x-0-75-mm-schwarz-meterware-1499078.html) |
| Schrumpfschlauch                       | 2      | 0,45 €   | [Link](https://www.conrad.de/de/p/tru-components-1564565-schrumpfschlauch-ohne-kleber-schwarz-3-mm-schrumpfrate-3-1-meterware-1564565.html) |
| Widerstandsset                         | 1      | 14,48 €  | [Link](https://www.conrad.de/de/p/barthelme-00430333-metallschicht-widerstand-sortiment-axial-bedrahtet-0207-0-25-w-1-400-st-1692551.html) |
| Breadboard Jumper                      | 1      | 2,95 €   | [Link](https://www.conrad.de/de/p/breadboard-jumper-wires-set-140-roots-14-kinds-for-arduino-802555851.html) |
| Breadboard                             | 2      | 4,73 €   | [Link](https://www.conrad.de/de/p/tru-components-0165-40-4-28010-steckplatine-bus-stripe-verschiebbar-polzahl-gesamt-400-l-x-b-x-h-86-5-x-64-5-x-8-5-mm-1572580.html) |
| Bananenstecker (Rot)                   | 5      | 1,79 €   | [Link](https://www.conrad.de/de/p/bkl-electronic-072149-p-bananenstecker-stecker-gerade-stift-4-mm-rot-1-st-730106.html) |
| Bananenstecker (Schwarz)               | 5      | 1,75 €   | [Link](https://www.conrad.de/de/p/bkl-electronic-072150-p-bananenstecker-stecker-gerade-stift-4-mm-schwarz-1-st-730114.html) |
| Bananenbuchse (Rot)                    | 5      |  0,87 €  | [Link](https://www.conrad.de/de/p/econ-connect-bak4srt-bananenstecker-buchse-gerade-stift-4-mm-rot-1-st-1405030.html) |
| Bananenbuchse (Schwarz)                | 5      | 0,95 €   | [Link](https://www.conrad.de/de/p/econ-connect-bak4sw-bananenstecker-buchse-gerade-stift-4-mm-schwarz-1-st-1303402.html) |
|                                        |        |          |            |
| Carrera Geraden (klein)                | 1      | 15,99 €  | [Link](https://www.conrad.de/de/p/carrera-20020612-evolution-digital-132-digital-124-1-4-geraden-1-paar-518135.html) |
| Raspberry Pi Button Modul              | 2      | 2,79 €   | [Link](https://www.conrad.de/de/p/druckschalter-se043-iduino-se043-1616261.html) |
| Raspberry Pi LED Modul                 | 3      | 2,78 €   | [Link](https://www.conrad.de/de/p/rgb-led-modul-se010-iduino-se010-1616253.html) |
| Bananenstecker (Schwarz)               | 5      | 1,75 €   | [Link](https://www.conrad.de/de/p/bkl-electronic-072150-p-bananenstecker-stecker-gerade-stift-4-mm-schwarz-1-st-730114.html) |
| Schraubendreherset                     | 1      | 34,99 €  | [Link](https://www.conrad.de/de/p/elektronik-u-feinmechanik-schraubendreher-set-12teilig-wera-schlitz-kreuzschlitz-phillips-innen-torx-innen-sechska-818892.html) |
| Schleifpapier                          | 1      |  7,95 €  | [Link](https://www.conrad.de/de/p/kwb-812329-handschleifpapier-set-koernung-180-240-400-600-l-x-b-280-mm-x-230-mm-20-st-812329.html) |
|                                        |        |          |            |

<!--
[This section describes any purchased components to be used with the system, any applicable licensing or usage restrictions, and any associated compatibility and interoperability or interface standards.]
-->

#### 3.9 Interfaces
tbd
<!-- 
[This section defines the interfaces that must be supported by the application. It should contain adequate specificity, protocols, ports and logical addresses, and the like, so that the software can be developed and verified against the interface requirements.]
-->

##### 3.9.1 User Interfaces
tbd
<!--
[Describe the user interfaces that are to be implemented by the software.]
-->

##### 3.9.2 Hardware Interfaces
The Raspberry Pi features many GPIO Hardware interfaces, which need to be connected like descriped below. 
Additionaly you are required to connect Ground and Power to any unconnected Pin. Furthermore you need to 
connect the modified power supply to the motor driver shield and back to the track. The Raspberry Pi needs 
to be connected to the standard power supply too.

| BCM Pin Number | Labeled Connector    |
|----------------|----------------------|
| 2              | LED 1 - Red          |
| 3              | LED 1 - Green        |
| 4              | LED 1 - Blue         |
| 17             | LED 2 - Red          |
| 27             | LED 2 - Green        |
| 22             | LED 2 - Blue         |
| 10             | LED 3 - Red          |
| 9              | LED 3 - Green        |
| 11             | LED 3 - Blue         |
| 14             | LED 4 - Red          |
| 15             | LED 4 - Green        |
| 18             | LED 4 - Blue         |
| 16             | LED 5 - Red          |
| 20             | LED 5 - Green        |
| 21             | LED 5 - Blue         |
| 19             | Startbutton          |
| 26             | Stopbutton           |
| 8              | Photoelectric Sensor |


<!--
[This section defines any hardware interfaces that are to be supported by the software, including logical structure, physical addresses, expected behavior, and so on.]
-->

##### 3.9.3 Software Interfaces
The webfrontend communicates with the backend by serializing a shared object 
to the file **/opt/arerrac/currentTrack.bin** and a simple systemctl service restart of arerrac-track service.
On start of the service it will load the shared object and configure the nessasary components to play with the track.
<!--
[This section describes software interfaces to other components of the software system. These may be purchased components, components reused from another application or components being developed for subsystems outside of the scope of this SRS but with which this software application must interact.]
-->

##### 3.9.4 Communications Interfaces
There are no direct communication interfaces required, besides connection a wireless device with the arerrac mi network.
<!--
[Describe any communications interfaces to other systems or devices such as local area networks, remote serial devices, and so forth.] 
-->

#### 3.10 Licensing Requirements
Licensee Information:
* [Polulu Motor Driver Board](https://github.com/pololu/dual-max14870-motor-driver-rpi/blob/master/LICENSE.txt)
* [Raspberry Pi](https://www.raspberrypi.org/trademark-rules/)
* [Carerra](https://www.carrera-toys.com/de/1363/lizenz-bersicht)

<!--
[Defines any licensing enforcement requirements or other usage restriction requirements that are to be exhibited by the software.]
-->

#### 3.11 Legal, Copyright, and Other Notices
tbd
<!--
[This section describes any necessary legal disclaimers, warranties, copyright notices, patent notices, wordmark, trademark, or logo compliance issues for the software.]
-->

#### 3.12 Applicable Standards
tbd
<!--
[This section describes by reference any applicable standard and the specific sections of any such standards which apply to the system being described. For example, this could include legal, quality and regulatory standards, industry standards for usability, interoperability, internationalization, operating system compliance, and so forth.]
-->

### 4. Supporting Information
tbd
<!--
[The supporting information makes the SRS easier to use.  It includes:
•	Table of contents
•	Index
•	Appendices
These may include use-case storyboards or user-interface prototypes. When appendices are included, the SRS should explicitly state whether or not the appendices are to be considered part of the requirements.]
-->
